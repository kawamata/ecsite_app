class Item < ApplicationRecord
	mount_uploader :image, ItemImageUploader
# ここ書いた後でサーバー再起動しないと、エラーになる

	validates :name, presence: true
	validates :description, presence: true
end