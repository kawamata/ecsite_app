class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  # before_action :authenticate_admin!

  before_action :configure_permitted_parameters, if: :devise_controller?

private
	def configure_permitted_parameters
		# オリジナルカラムの登録を許可
		devise_parameter_sanitizer.permit(:sign_up, keys: [:username])
		# オリジナルカラムの変更を許可
		devise_parameter_sanitizer.permit(:account_update, keys: [:username])
	end

end
