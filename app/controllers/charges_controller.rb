class ChargesController < ApplicationController
  def new
  end

  def create
    amount = 0
    (session[:cart] || []).each do |item_id|
      # amount = amount + Item.find(item_id).price　ということです
      amount += Item.find(item_id).price
    end
    # 以下のコードの意味
    customer = Stripe::Customer.create(
      email: params[:stripeEmail],
      source: params[:stripeToken]
      )
    Stripe::Charge.create(
      customer: customer.id,
      amount: amount,
      currency: 'jpy'
      )
    # 決済が終わったら、セッションカートを空にする
    session[:cart] = []
  end
end
