class HomeController < ApplicationController

	before_action :authenticate_user!, only: :top

  def top
  	@items = Item.all.reverse
  end

  def index
  end
end
