class CartController < ApplicationController
  def index
    # @items = []
    # (session[:cart] || []).each do |item_id|
    #   @items << Item.find(item_id)
    # end
    @items = (session[:cart] || []).map { |item_id| Item.find(item_id) }
    # binding.pry
    @sum = (@items.map{|item| item.price}).sum
    # binding.pry
  end

  def create
    # セッションに値を保存する
    # sessionを空の配列とし、もしsession[:cart]が存在してたら、これを作らない（データがなくなってまう）
    session[:cart] = [] unless session[:cart]
    session[:cart] << params[:item_id]
    redirect_to top_path
  end

  def update
    # 一つだけ商品を削除
    @items = session[:cart].delete_at(session[:cart].find_index(params[:id]))
    # binding.pry
    redirect_to cart_index_path
  end

  def destroy
    session[:cart] = []
    # カートの中身を削除してから、改めてカート変数を作成するために、indexアクションへ飛ばす（または、カートページにてカートの中身を判定するためとも言える）
    redirect_to action: 'index'
  end
end
