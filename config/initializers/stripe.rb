# https://qiita.com/100010/items/c09fb08dd555b32a2652
# initializers触ったら、webサーバーをリスタートしないと読み込まれない

Rails.configuration.stripe = {
    publishable_key: ENV['STRIPE_PUB'],
    secret_key: ENV['STRIPE_SEC'],

    # secret.yml使う場合
    # publishable_key: Rails.application.secrets.stripe_publishable_key,
    # secret_key: Rails.application.secrets.stripe_secret_key
}
Stripe.api_key = Rails.configuration.stripe[:secret_key]

# secret.yml使う場合
# Stripe.api_key = Rails.application.secrets.stripe_secret_key
