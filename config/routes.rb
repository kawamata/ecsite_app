Rails.application.routes.draw do
  devise_for :users
  root 'home#index'
  get '/top', to: 'home#top'
  # get 'home/top'

  # get 'cart/index'
  # get 'cart/create'

  # get 'charges/new'
  # get 'charges/create'

  resources :items
  resources :cart
  resources :charges, only: [:new, :create]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
