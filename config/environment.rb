# Load the Rails application.
require_relative 'application'

# 以下、carrierwave追加後のrails c でエラーとなり、追加したコード
require 'carrierwave/orm/activerecord'

# Initialize the Rails application.
Rails.application.initialize!
